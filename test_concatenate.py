import concatenate

def test_concatenate():
    # Testing if the concatenation of strings is working
    assert concatenate.concatenate("git","lab") == "gitlab"

    # Testing if the concatenation of numbers is working
    assert concatenate.concatenate(1,2) == "12"

if __name__ == "__main__":
    test_concatenate()
    print("Everything passed")
    